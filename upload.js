const name = 'phys1';

const request = require('request-promise-native')/*.defaults({
	rejectUnauthorized: false,
	proxy: 'http://127.0.0.1:8888'
});*/

const path = require('path');
const fs = require('fs');

const auth = require('./auth.json');
const parse = require('csv-parse/lib/sync')
const stringify = require('csv-stringify/lib/sync')

let header;
const files = [];
parse(fs.readFileSync(path.resolve('./edited', name, 'files.csv'))).forEach((f, i)=>{
	if(!i){
		header = f;
		return;
	}
	const file = {};
	header.forEach((field, index)=>{
		file[field] = f[index];
	})
	files.push(file);
});

console.log(files)

function upload(){
	const file = files.pop();
	if(!file){
		return;
	}
	const actualFiles = fs.readdirSync(path.resolve('./edited', name, 'files', file.nid)).filter(item => !(/(^|\/)\.[^\/\.]/g).test(item)).map(fileName=>{
		return {
			value: fs.readFileSync(path.resolve('./edited', name, 'files', file.nid, fileName)),
			options: {
				filename: fileName
			}
		}
	})
	
	const options = {
		method: 'POST',
		url: 'https://api.amiv.ethz.ch/studydocuments',
		formData: {
			course_year: file.year,
			lecture: file.lecture,
			title: file.description,
			type: file.type,
			department: file.departement,
			semester: file.semester,
			files: actualFiles
		},
		headers: {
			authorization:	auth
		}
	};
	if(file.author.length){
		options.formData.author = file.author;
	}
	if(file.prof.length){
		options.formData.professor = file.prof;
	}
	console.log(options)
	request(options).then(()=>{
		console.log(`uploaded ${file.description} ${file.semester} ${file.year}`)
		upload();
	})
}


upload();