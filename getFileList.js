const files = [];
Array.from(document.querySelectorAll('table')).forEach(table=>{
	const caption = table.querySelector('caption').innerHTML;
	Array.from(table.querySelectorAll('tr')).forEach(row=>{
		const cells = row.querySelectorAll('td');
		if(!cells.length){
			return;
		}
		files.push({
			caption,
			nid: cells[0].innerHTML.trim(),
			description: cells[1].innerHTML.trim(),
			year: cells[2].innerHTML.trim(),
			author: cells[3].innerHTML.trim(),
			files: Array.from(cells[4].querySelectorAll('a')).map(f=>({
				href: f.getAttribute('href'),
				title: f.getAttribute('title'),
			}))
		})
	})
})
console.log(JSON.stringify(files));