const fs = require('fs');
const path = require('path');
const request = require('request-promise-native')/*.defaults({
	rejectUnauthorized: false,
	proxy: 'http://127.0.0.1:8888'
});*/

const files = require('./files.json');
const cookies = require('./cookies.json');


const threads = 10;
const downloadQueue = [];


files.forEach(f=>{
	f.files.forEach((file, index)=>{
		downloadQueue.push({
			href: file.href,
			nid: f.nid,
			title: file.title || file.href.split('/').pop()
		})
	})
})

function download(){
	const file = downloadQueue.pop();
	if(!file){
		return;
	}
	request({
		url: file.href,
		headers: {
			cookie: cookies.map(c=>`${c.name}=${c.value}`).join('; ')
		},
		encoding: null
	}).then(body=>{
		try {
			fs.mkdirSync(path.resolve('./files', file.nid))
		}catch(e){

		}
		fs.writeFile(path.resolve('./files', file.nid, file.title), body, ()=>{});
		console.log(`downloaded ${file.nid} ${file.title}`);
		download();
	})
}


for(let i = 0; i < threads; i++){
	download();
}