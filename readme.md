0. make sure node and NPM is installed, don't forget "npm install"

1. create "cookies.json" with your https://www.amiv.ethz.ch/studium/unterlagen cookies (use EditThisCookie extension export function in chrome)
2. create "auth.json" with your (double quoted) auth token from future.amiv.ethz.ch

3. paste getFileList.js's content in the developer console while on https://www.amiv.ethz.ch/studium/unterlagen/88 or whatever course you want to export
4. save the console logged contents as "files.json"
5. node genCSV && node download
6. edit the csv document to match the required format (file0-9 columns are for informative purposes only)
7. rename all files in files folder to match format (you can use "nid" in csv to find them)
8. create a new folder named "edited" and within that a subfolder containing your edited "files" folder AND files.csv (you can name it according to the course)
9. enter said name in upload.js line 1
10. run upload.js (node upload)
11. profit.