let files = require('./files.json');
const fs = require('fs');

const parse = require('csv-parse/lib/sync')
const stringify = require('csv-stringify/lib/sync')

function getFiles(f){
	const files = {};
	f.files.forEach((f, index)=>{
		files['file'+index] = f.title || f.href.split('/').pop()
	})
	return files;
}

const captionMap = {
	'Prüfungen': 'exams',
	'Skripts und ähnliches': 'lecture documents',
	'Übungsserien und ähnliches': 'exercises',
	'Sonstiges': '',
	'Zusammenfassungen': 'cheat sheets',
	'Testatklausuren': 'exams'
}

files = files.map(f=>({
	departement: '',
	lecture: '',
	...f,
	type: captionMap[f.caption],
	prof: f.author,
	semester: '',
	file0: '',
	file1: '',
	file2: '',
	file3: '',
	file4: '',
	file5: '',
	file6: '',
	file7: '',
	file8: '',
	file9: '',
	...getFiles(f)
}))

fs.writeFile('files.csv', stringify(files, {
	header: true,
  	columns: {
  		departement: 'departement',
  		lecture: 'lecture',
   		type: 'type',
   		nid: 'nid',
   		description: 'description',
   		year: 'year',
   		semester: 'semester',
   		author: 'author',
   		prof: 'prof',
   		file0: 'file0',
   		file1: 'file1',
   		file2: 'file2',
   		file3: 'file3',
   		file4: 'file4',
   		file5: 'file5',
   		file6: 'file6',
   		file7: 'file7',
   		file8: 'file8',
   		file9: 'file9',
  	}
}), ()=>{});